from django.db.models import ForeignKey, CASCADE, TextField, CharField

from django.utils.translation import ugettext as _

from api.models.base import BaseModel
from api.models.hobby import Hobby
from api.models.language import Language
from api.models.profile import Profile
from api.models.recomendation import Recommendation
from api.models.training import Trainer


class AboutMe(BaseModel):
    profile_id = ForeignKey(
        Profile,
        on_delete=CASCADE,
        verbose_name=_('Id Perfil'),
        db_column='profile_id',
        editable=True,
        help_text=_('Id del proyecto')
    )

    title = CharField(
        max_length=50,
        blank=True, default='',
        verbose_name=_('Titulo'),
        db_column='title'
    )

    content = TextField(
        blank=True, default='',
        verbose_name=_('Contenido'),
        db_column='content'
    )

    trainer_id = ForeignKey(
        Trainer,
        on_delete=CASCADE,
        verbose_name=_('Formación Academica'),
        db_column='trainer_id'
    )

    hobbies_id = ForeignKey(
        Hobby,
        on_delete=CASCADE,
        verbose_name=_('Hobies'),
        db_column='hobbies_id'
    )

    language_id = ForeignKey(
        Language,
        on_delete=CASCADE,
        verbose_name=_('Idiomas'),
        db_column='language_id'
    )

    recommendation_id = ForeignKey(
        Recommendation,
        on_delete=CASCADE,
        verbose_name=_('Recomendaciones'),
        db_column='recommendation_id'
    )

    class Meta:
        verbose_name = "About Me"
        verbose_name_plural = "About Me"
