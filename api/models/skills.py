from django.db.models import ForeignKey, CASCADE, CharField, ImageField, TextField

from api.models.base import BaseModel
from api.models.profile import Profile


class GroupsOrganizations(BaseModel):
    sub_group_id = ForeignKey('self', on_delete=CASCADE, null=True, blank=True)
    title = CharField(max_length=50)


class Skill(BaseModel):
    profile_id = ForeignKey(Profile, on_delete=CASCADE)
    group_id = ForeignKey(GroupsOrganizations, on_delete=CASCADE)
    title = CharField(max_length=50)
    image = ImageField(upload_to='img/skills')
    description = TextField(blank=True, null=True)
