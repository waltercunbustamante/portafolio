from django.db.models import TextField, CharField, ForeignKey, CASCADE

from api.models import BaseModel


class GroupLink(BaseModel):
    types = CharField(max_length=5)


class Link(BaseModel):
    types = ForeignKey(GroupLink, on_delete=CASCADE)
    url = TextField()
