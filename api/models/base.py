import re
from django.db.models import Model, CharField


class BaseModel(Model):
    LANGUAGES = (
        ('en', 'English'),
        ('es', 'Español'),
    )

    code_lang = CharField(max_length=2, choices=LANGUAGES, default='es')

    class Meta:
        abstract = True

    def save(self, *args, **kwargs):
        name = re.findall('[A-Z]*', self.__class__.__name__)
        self._meta.db_table = '_'.join(word.lower() for word in name)
        super(BaseModel, self).save(*args, **kwargs)
