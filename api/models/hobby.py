from django.db.models import ImageField, ForeignKey, CASCADE, TextField, CharField

from api.models.base import BaseModel
from api.models.profile import Profile


class Hobby(BaseModel):
    profile_id = ForeignKey(Profile, on_delete=CASCADE)
    icon = ImageField(upload_to='img/hobbies')
    title = CharField(max_length=50)
    description = TextField()
