from django.db.models import Model, CharField, ImageField, DateTimeField, ForeignKey, CASCADE

from api.models.base import BaseModel
from api.models.profile import Profile


class Project(BaseModel):
    profile_id = ForeignKey(Profile, on_delete=CASCADE)
    photo = ImageField(upload_to='img/proyects')
    title = CharField(max_length=50)
    details = CharField(max_length=50)
    date_start = DateTimeField()
    date_update = DateTimeField()
    url_repository = CharField(max_length=50)
