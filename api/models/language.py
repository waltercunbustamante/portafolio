from django.db.models import ForeignKey, CASCADE, CharField

from api.models.base import BaseModel
from api.models.profile import Profile


class Language(BaseModel):
    profile_id = ForeignKey(Profile, on_delete=CASCADE)
    language = CharField(max_length=50)
