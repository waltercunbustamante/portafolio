from django.db.models import ForeignKey, CASCADE, CharField, ImageField, DateField

from api.models.base import BaseModel
from api.models.profile import Profile


class Certification(BaseModel):
    profile_id = ForeignKey(Profile, on_delete=CASCADE)
    title = CharField(max_length=50)
    issuing_company = CharField(max_length=50)
    expedition_date = CharField(max_length=50)
    date_expiry = DateField()
    id_cert = CharField(max_length=50)
    url_cert = CharField(max_length=50)
    image = ImageField(upload_to='img/certifications')
