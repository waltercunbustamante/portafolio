from django.db.models import TextField, ForeignKey, DateField, CharField, CASCADE

from api.models.base import BaseModel
from api.models.profile import Profile


class Recommendation(BaseModel):
    profile_id = ForeignKey(Profile, on_delete=CASCADE)
    name = CharField(max_length=50)
    date_created = DateField()
    description = TextField()
    url = TextField()
