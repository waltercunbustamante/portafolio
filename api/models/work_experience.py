from django.db.models import Model, ForeignKey, CASCADE, DateTimeField, TextField, CharField, ImageField, BooleanField, \
    ManyToManyField

from api.models import Aptitud
from api.models.base import BaseModel
from api.models.profile import Profile


class JobTitle(BaseModel):
    name = CharField(max_length=50)


# ---------------------------------------------------------------------------------------------------------------------

class JobType(BaseModel):
    # Tipo de Empleo (Jornada Completa, parcial, Contrato Temporal, Temporal)
    name = CharField(max_length=50)


# ---------------------------------------------------------------------------------------------------------------------


class TypeOrganization(BaseModel):
    # Tipo de Entidad (Empresa, Organizacion, Cooperativa, Pyme, Fundacion)
    name = CharField(max_length=25)


# ---------------------------------------------------------------------------------------------------------------------
class SectorCompany(BaseModel):
    # Sector de la compania (Banca, telecomunicaciones, Servicios Finacieros, etc)
    name = CharField(max_length=25)


class Company(BaseModel):
    # Datos de la Empresa
    name = CharField(max_length=50)
    logo = ImageField(upload_to='img/companies')
    type_org = ForeignKey(SectorCompany, on_delete=CASCADE)
    sector = CharField(max_length=50)


# ---------------------------------------------------------------------------------------------------------------------

class Localization(BaseModel):
    # Instancia de localization de una entidad
    city = CharField(max_length=50)
    province = CharField(max_length=50)
    country = CharField(max_length=50)


# ---------------------------------------------------------------------------------------------------------------------

class ModeWork(BaseModel):
    # Modo de Trabajo (remoto, presencial, hibrido)
    name = CharField(max_length=50)
    is_active = BooleanField(default=True)


class WorkExperience(BaseModel):
    profile_id = ForeignKey(Profile, on_delete=CASCADE)
    job_title = ForeignKey(JobTitle, on_delete=CASCADE)  # Cargo Laboral
    company_id = ForeignKey(Company, on_delete=CASCADE)  # Nombre de la Empresa
    location_id = ForeignKey(Localization, on_delete=CASCADE)  # Ubicacion de cuidad o pais
    mode_work_id = ForeignKey(ModeWork, on_delete=CASCADE)  # Modalidad de Trajo
    is_working = BooleanField(default=False)  # Estoy trabajando actualmente
    start_date_month = CharField(max_length=2)  # Mes de inicio
    start_date_year = CharField(max_length=4)  # Año de inicio
    end_date_month = CharField(max_length=2)  # Mes de Finalización
    end_date_year = CharField(max_length=4)  # Año de Finalización
    # is_finished_work = BooleanField(default=False)
    sector = ForeignKey(SectorCompany, on_delete=CASCADE)
    description = TextField(blank=True, default='')
    aptitude = ManyToManyField(Aptitud, related_name='work_experience_aptitude', verbose_name='Aptitude', blank=True)
