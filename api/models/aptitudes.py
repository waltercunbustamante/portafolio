from django.db.models import ForeignKey, CASCADE
from django.forms import CharField

from api.models.base import BaseModel
from api.models.profile import Profile


class Aptitud(BaseModel):
    profile_id = ForeignKey(Profile, on_delete=CASCADE)
    name = CharField()
