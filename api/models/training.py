from django.db.models import CharField, ForeignKey, CASCADE, TextField, DateField

from api.models.base import BaseModel
from api.models.profile import Profile


class EducationalInstitution(BaseModel):
    profile_id = ForeignKey(Profile, on_delete=CASCADE)
    title = CharField(max_length=10, unique=True)
    academic_discipline = CharField(max_length=10)
    date_start = DateField()
    date_end = DateField()
    note = TextField()
    # activities = ForeignKey


class Trainer(BaseModel):
    profile_id = ForeignKey(Profile, on_delete=CASCADE)
    obtained_title = CharField(max_length=50)
    educational_institution_id = ForeignKey(EducationalInstitution, on_delete=CASCADE)
