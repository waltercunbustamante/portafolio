from django.db.models import Model, TextField, ForeignKey, CASCADE

from api.models.profile import Profile


class Introduce(Model):
    profile_id = ForeignKey(Profile, on_delete=CASCADE)
    title = TextField()
    content = TextField()
