from django.contrib import admin
from django.contrib.admin import ModelAdmin

from api.models import Profile, AboutMe, Aptitud, Certification, Hobby, Introduce, Language, Project, Recommendation, \
    GroupsOrganizations, Link, EducationalInstitution, Trainer, JobTitle, JobType, TypeOrganization, SectorCompany, \
    Company, WorkExperience, ModeWork, Localization


# -----------------------------------------------------------------------------------------------------------

@admin.register(Profile)
class ProfileAdmin(ModelAdmin):
    pass


# -----------------------------------------------------------------------------------------------------------

@admin.register(AboutMe)
class AboutMeAdmin(ModelAdmin):
    pass


# -----------------------------------------------------------------------------------------------------------

@admin.register(Aptitud)
class AptitudAdmin(ModelAdmin):
    pass


# -----------------------------------------------------------------------------------------------------------

@admin.register(Certification)
class CertificationAdmin(ModelAdmin):
    pass


# -----------------------------------------------------------------------------------------------------------

@admin.register(Hobby)
class HobbyAdmin(ModelAdmin):
    pass


# -----------------------------------------------------------------------------------------------------------


@admin.register(Introduce)
class IntroduceAdmin(ModelAdmin):
    pass


# -----------------------------------------------------------------------------------------------------------


@admin.register(Language)
class LanguageAdmin(ModelAdmin):
    pass


# -----------------------------------------------------------------------------------------------------------

@admin.register(Project)
class ProjectAdmin(ModelAdmin):
    pass


# -----------------------------------------------------------------------------------------------------------

@admin.register(Recommendation)
class RecommendationAdmin(admin.ModelAdmin):
    pass


# -----------------------------------------------------------------------------------------------------------

@admin.register(GroupsOrganizations)
class GroupsOrganizationsAdmin(admin.ModelAdmin):
    pass


# -----------------------------------------------------------------------------------------------------------

@admin.register(Link)
class LinkAdmin(ModelAdmin):
    pass


# -----------------------------------------------------------------------------------------------------------

@admin.register(EducationalInstitution)
class EducationalInstitutionAdmin(ModelAdmin):
    pass


# -----------------------------------------------------------------------------------------------------------

@admin.register(Trainer)
class TrainerAdmin(ModelAdmin):
    pass


# -----------------------------------------------------------------------------------------------------------
@admin.register(JobTitle)
class JobTitleAdmin(ModelAdmin):
    pass


@admin.register(JobType)
class JobTypeAdmin(ModelAdmin):
    pass


@admin.register(TypeOrganization)
class TypeOrganizationAdmin(admin.ModelAdmin):
    pass


@admin.register(SectorCompany)
class SectorCompanyAdmin(admin.ModelAdmin):
    pass


@admin.register(Company)
class CompanyAdmin(admin.ModelAdmin):
    pass


@admin.register(Localization)
class LocalizationAdmin(admin.ModelAdmin):
    pass


@admin.register(ModeWork)
class ModeWorkAdmin(admin.ModelAdmin):
    pass


@admin.register(WorkExperience)
class WorkExperienceAdmin(admin.ModelAdmin):
    pass
