from django.apps import AppConfig


class PortafolioConfig(AppConfig):
    name = 'briefcase'
