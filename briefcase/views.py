from pathlib import Path

from django.http import HttpResponse
from django.shortcuts import render

from django.utils.translation import ugettext as _
from django.utils.translation.trans_real import get_languages


def index(request):
    """
    Función que da soporte a diferentes idiomas en la aplicación web.

    Parameters:
    request (objeto HTTP): Es un objeto Django que contiene muchos tipos de
    información acerca de la petición HTTP realizada.

    Proceso:
    - Obtiene una lista de idiomas soportados.
    - Para cada idioma en la lista, genera una ruta a la imagen de la bandera correspondiente.
    - Retorna la página 'index.html' con el diccionario de idiomas y rutas de imagen correspondientes.

    Retorno:
    render (objeto HTTPResponse): Retorna una instancia de HTTPResponse que contiene 'index.html'
    y un contexto que incluye el dicionario 'languages', donde la clave es el idioma y el valor es la ruta de la imagen.
    """
    languages = list[dict[str, str]]()
    flags = [file.stem for file in Path(r'briefcase/static/img/flags').iterdir() if file.is_file()]
    for lang in get_languages():
        if lang in flags:
            languages.append({
                'code': lang,
                'flag_url': f'img/flags/{lang}.webp'
            })
        else:
            languages.append({
                'code': lang,
                'flag_url': f'img/flags/other.webp'
            })
    return render(request, 'index.html', {'languages': languages})
