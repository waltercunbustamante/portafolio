# Funcionalidades
- Soporte multilenguaje
- Dark Theme


* CV Descarga (Boton en Presentacion)
* Contactame (Boton en Presentacion)

# Estructura de Portafolio
1. Presentacion
   2. Foto
   3. Titulo
   3. Contenido
4. Experiencia Laboral
   5. Cargo
   6. Tipo de Empleo
   7. Nombre de Empresa
   8. Ubicacion
   9. Fecha de Inicio
   10. Fecha Fin
   11. Sector
   12. Descripcion
   13. Aptitudes y Conocimientos
5. Proyectos
   6. photo
   7. title
   8. details
   9. date start
   10. date update
5. Sobre mi
   6. titulo
   7. Contenido
   8. Formacion
      9. Institucion Educativa
      10. Titulo
      11. Disciplina Academica
      12. Fecha de Inicio
      13. Fecha Finalizacion (o fecha prevista)
      14. Nota
      15. Actividades y grupos
   9. Hobbies
      10. Icon
      11. titulo
      12. descripcion
   10. Idiomas
      10. Idioma
      11. Competencia
   10. Recomendaciones
       11. Nombre
       12. Fecha
       13. Descripcion
       12. Link
   6. Git Stats 

8. Skill / Conocimientos
   7. Tools
   8. Data Base
   8. Front-End
   8. Back-End
   9. Game Developer
   10. Web Desing
   11. Low Code
   12. Cloud Services
   12. Other Skills
7. Certificaciones
   8. Titulo
   9. Empresa Emisora
   10. Fecha de Expedicion
   11. Fecha de Caducidad
   12. ID Credencial
   13. URL de Credencial
   14. Imagen
8. Aptitudes
   9. id 
   10. Nombre
11. Bloc
    12. -------
9. Contacto
   10. Nombre Completo
   11. Correo
   12. Asunto
   13. Contenido



# Pie de Pagina
- linkeding
- Gitlab
- Correo Profesional
- Whatsapp